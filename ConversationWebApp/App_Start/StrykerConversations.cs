using Stryker.Conversations.Web;
using System;
using System.Web.Mvc;

[assembly: WebActivatorEx.PostApplicationStartMethod(
    typeof(ConversationWebApp.App_Start.StrykerConversations), "PostStart")]

namespace ConversationWebApp.App_Start {
    public static class StrykerConversations {
        public static void PostStart() {
            GlobalFilters.Filters.Add(new ConversationFilter());
        }
    }
}