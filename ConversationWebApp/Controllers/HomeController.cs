﻿using ConversationWebApp.Flows;
using Stryker.Conversations;
using Stryker.Conversations.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConversationWebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";
            
            return View();
        }

        [HttpPost]
        [BeginConversation(Flow=typeof(SampleFlow))]
        public ActionResult Index(string userText)
        {
            var conversation = Conversation.Current;
            conversation["Message"] = userText;
            return RedirectToAction("Index2");
        }

        [RequireConversation(Action = "Index", Flow = typeof(SampleFlow))]
        public ActionResult Index2()
        {
            return View();
        }

        [RequireConversation(Action = "Index", Flow = typeof(SampleFlow))]
        [HttpPost]        
        public ActionResult Index2(string submit)
        {
            return RedirectToAction("Index3");
        }

        [RequireConversation(Action = "Index", Flow = typeof(SampleFlow))]
        public ActionResult Index3()
        {
            return View();
        }

        [RequireConversation(Action = "Index", Flow = typeof(SampleFlow))]
        [EndConversation]
        public ActionResult Index4()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
