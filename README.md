# Stryker.Conversations

An implementation of the concept of a Conversation scope for ASP.NET MVC3.

A Conversation is a key-value store similar to the HTTPSessionState but that differs in two ways:

1. A conversation has a shorter life cycle that is explicitly demarcated by the application developer.
2. A user may have multiple active conversations (e.g. one for each browser window or tab).

A Conversation represents a single task from a user perspective and holds state associated with the current task that the user is working on.  If the user is working on multiple tasks concurrently then they will have multiple conversations running.