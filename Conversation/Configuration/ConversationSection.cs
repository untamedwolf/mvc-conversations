﻿namespace Stryker.Conversations.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// The configuration properties used by this library.  This is the materialization
    /// of the properties from Web.config.
    /// </summary>
    public class ConversationSection : ConfigurationSection
    {
        /// <summary>
        /// Gets the name of the request parameter that will hold the Conversation ID
        /// </summary>
        [ConfigurationProperty("requestParameter", IsRequired = false)]
        public string RequestParameter 
        { 
            get 
            { 
                return base["requestParameter"] as string; 
            } 
        }

        /// <summary>
        /// Gets the name of the view bag parameter that will store the current Conversation ID
        /// </summary>
        [ConfigurationProperty("viewBagParameter", IsRequired = false)]
        public string ViewBagParameter 
        { 
            get 
            { 
                return base["viewBagParameter"] as string; 
            } 
        }

        /// <summary>
        /// Gets the default timeout for a conversation (i.e. the amount of time that must elapse
        /// with a conversation unused before it may be garbage-collected).
        /// </summary>
        [ConfigurationProperty("timeout", IsRequired = false)]
        public int? Expiry
        {
            get
            {
                return base["timeout"] as int?;
            }
        }
    }
}
