namespace Stryker.Conversations.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;
    using Stryker.Conversations;

    /// <summary>
    /// String constants and configuration properties used internally to manage conversations.
    /// </summary>
    public class ConversationConfiguration
    {
        /// <summary>
        /// The namespace of this class; used as a unique name to qualify various keys
        /// </summary>
        private static readonly string NAMESPACE = typeof(ConversationConfiguration).Namespace;

        /// <summary>
        /// The HttpContent Item key that the ConversationFilter will place the current conversation in
        /// so that it can be retrieved by the user from Conversation.Current
        /// </summary>
        public static readonly string HTTPCONTEXT_ITEM = NAMESPACE + ".Conversation";
        
        /// <summary>
        /// The name of the Request Parameter that contains a Conversation ID
        /// </summary>
        public static readonly string REQUEST_PARAMETER;
        
        /// <summary>
        /// The amount of time a conversation must remained untouched to timeout (in minutes)
        /// This may not be exactly honored since conversations are only cleaned up when either
        /// the session expires or a conversational page is accessed
        /// </summary>
        public static readonly TimeSpan EXPIRY; // in minutes

        /// <summary>
        /// A template for the key that will be used in the conversation backing store when
        /// an item is stored in the conversation
        /// </summary>
        public static readonly string CONVERSATION_STORE_TEMPLATE = NAMESPACE + "|{0}|{1}";
        
        /// <summary>
        /// A template for the key that will be used in the session to store the map of active
        /// conversations and their last access times.
        /// </summary>
        public static readonly string CONVERSATION_ACTIVE_TEMPLATE = NAMESPACE + "|_ActiveConversations";
       
        /// <summary>
        /// A regular expression to quickly find the conversation last accessed keys in the conversation
        /// </summary>
        public static readonly Regex CONVERSATION_LASTACCESSED_REGEX = new Regex(NAMESPACE.Replace(".", "\\.") + "\\|(id:[^|]+)\\|_LastAccessed");
        
        /// <summary>
        /// The group in CONVERSATION_LASTACCESSED_REGEX that contains the conversation id
        /// </summary>
        public static readonly string CONVERSATION_LASTACCESSED_REGEX_KEY_GROUP = "id";
        
        /// <summary>
        /// The character used to separate segments of the conversation store backing keys
        /// </summary>
        public static readonly char CONVERSATION_STORE_SEPARATOR = '|';
        
        /// <summary>
        /// The ViewBag key that will hold the current conversation id.
        /// </summary>
        public static readonly string CURRENT_CONVERSATION;
        
        /// <summary>
        /// The key in the conversation backing store that will hold the timeout
        /// </summary>
        public static readonly string CONVERSATION_TIMEOUT = "_" + NAMESPACE + ".TimeOut";

        /// <summary>
        /// The key in the conversation backing store that will hold the list of used keys
        /// </summary>
        public static readonly string CONVERSATION_USEDKEYS = "_" + NAMESPACE + ".UsedKeys";
        
        /// <summary>
        /// The key in the conversation backing store that will hold the flow
        /// </summary>
        public static readonly string CONVERSATION_FLOW = "_" + NAMESPACE + ".Flow";
        
        /// <summary>
        /// Template for The key in the conversation backing store that will hold the specified type
        /// </summary>
        public static readonly string CONVERSATION_TYPE_TEMPLATE = "_" + NAMESPACE + ".Type|{0}";

        /// <summary>
        /// The key used to store whether or not the redirect should propagate the conversation in the HttpContext
        /// </summary>
        public static readonly string CONVERSATION_PROPAGATE_ON_REDIRECT = NAMESPACE + ".PropagateOnRedirect";

        /// <summary>
        /// The configuration property in App.config or Web.config that will hold the Request Parameter name
        /// </summary>
        public static readonly string CONFIG_REQUEST_PARAMETER = NAMESPACE + ".RequestParameter";

        /// <summary>
        /// The configuration property in App.config or Web.config that will hold the View Bag Parameter name
        /// </summary>
        public static readonly string CONFIG_VIEWBAG_PARAMETER = NAMESPACE + ".ViewBagParameter";

        /// <summary>
        /// The configuration property in App.config or Web.config that will hold the default conversation timeout.
        /// </summary>
        public static readonly string CONFIG_TIMEOUT = NAMESPACE + ".TimeOut";

        /// <summary>
        /// Initializes static members of the <see cref="ConversationConfiguration"/> class.
        /// </summary>
        static ConversationConfiguration()
        {
            // read the configuration properties from app settings
            string requestParameter =
                string.IsNullOrEmpty(ConfigurationManager.AppSettings[CONFIG_REQUEST_PARAMETER])
                ?
                null
                :
                ConfigurationManager.AppSettings[CONFIG_REQUEST_PARAMETER];

            string currentConversation =
                string.IsNullOrEmpty(ConfigurationManager.AppSettings[CONFIG_VIEWBAG_PARAMETER])
                ?
                null
                :
                ConfigurationManager.AppSettings[CONFIG_VIEWBAG_PARAMETER];

            int expiry = 0;
            int.TryParse(ConfigurationManager.AppSettings[CONFIG_TIMEOUT], out expiry);

            // override any application settings with strongly typed ones if provided
            ConversationSection conversationSection = ConfigurationManager.GetSection("conversations") as ConversationSection;
            if (conversationSection != null)
            {
                requestParameter = string.IsNullOrEmpty(conversationSection.RequestParameter) ? requestParameter : conversationSection.RequestParameter;
                currentConversation = string.IsNullOrEmpty(conversationSection.ViewBagParameter) ? currentConversation : conversationSection.ViewBagParameter;
                expiry = conversationSection.Expiry.HasValue ? conversationSection.Expiry.Value : expiry;
            }

            // set the values from config file; or set to defaults
            TimeSpan? expiryTimespan = expiry <= 0 ? null : new TimeSpan(0, expiry, 0) as TimeSpan?;
            REQUEST_PARAMETER = requestParameter == null ? "conversationId" : requestParameter;
            CURRENT_CONVERSATION = currentConversation == null ? "ConversationId" : currentConversation;
            EXPIRY = expiryTimespan.HasValue ? expiryTimespan.Value : new TimeSpan(0, 30, 0);
        }
    }
}