namespace Stryker.Conversations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Stryker.Conversations.Configuration;
    using Stryker.Conversations.Exception;

    /// <summary>
    /// Represents a manager for bags of values whose lifecycle spans multiple requests; the precise
    /// lifecycle is explicitly demarcated either programmatically or declaratively by
    /// the use of attributes on Action methods.
    /// </summary>
    public static class Conversation
    {
        /// <summary>
        /// Gets the currently active conversation when called from an Action or ActionFilter.  
        /// The ConversationFilter must be installed as a Global Filter for this property to 
        /// function correctly.
        /// </summary>
        public static IConversation Current
        {
            get
            {
                return HttpContext.Current.Items[ConversationConfiguration.HTTPCONTEXT_ITEM] as IConversation;
            }
        }

        /// <summary>
        /// Gets the unique identifier of the current conversation
        /// </summary>
        public static Guid Id
        {
            get
            {
                return Current.Id;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the current conversation will end at 
        /// the completion of the next response; or if the conversation will
        /// only end when Conversation.End() is called.
        /// </summary>
        public static bool Transient
        {
            get
            {
                return Current.Transient;
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets a value indicating whether a redirect from this request 
        /// should continue inside the current conversation or exit the conversation 
        /// (without ending the conversation; call .End() to end the conversation)
        /// </para>
        /// <para>
        /// This value is only for the current request; and resets to
        /// true at the beginning of each new request.
        /// </para>
        /// </summary>
        public static bool PropagateOnRedirect
        {
            get
            {
                bool redirect = true;
                if (HttpContext.Current.Items[ConversationConfiguration.CONVERSATION_PROPAGATE_ON_REDIRECT] is bool)
                {
                    redirect = (bool)HttpContext.Current.Items[ConversationConfiguration.CONVERSATION_PROPAGATE_ON_REDIRECT];
                }

                return redirect;
            }

            set
            {
                HttpContext.Current.Items[ConversationConfiguration.CONVERSATION_PROPAGATE_ON_REDIRECT] = value;
            }
        }

        /// <summary>
        /// Gets or sets the amount of time the current conversation may be left untouched before it
        /// is garbage-collected.
        /// </summary>
        public static TimeSpan TimeOut
        {
            get
            {
                return Current.TimeOut;
            }

            set
            {
                Current.TimeOut = value;
            }
        }

        /// <summary>
        /// Gets or sets the Type marker for the current conversation.
        /// </summary>
        public static Type Flow
        {
            get
            {
                return Current.Flow;
            }

            set
            {
                Current.Flow = value;
            }
        }

        /// <summary>
        /// Get the non-transient conversation with the provided conversationId.
        /// If no such conversation exists and create is set to true then this method
        /// returns a new transient conversation with the specified id.  If it is
        /// set to false this method returns a NoSuchConversationException.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="create"></param>
        /// <returns></returns>
        public static IConversation Get(Guid conversationId, bool create = false)
        {
            return new ConversationInstance(((ConversationInstance)Current).Controller, conversationId, create);
        }

        /// <summary>
        /// Try to get the non-transient conversation with the provided conversationId.
        /// If no such conversation exists and create is set to true then conversation
        /// will become a new transient conversation with the specified id.  If it is
        /// set to false then conversation will be null.
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="create"></param>
        public static void TryGet(Guid conversationId, bool create, out IConversation conversation)
        {
            try
            {
                conversation = Get(conversationId, create);
            }
            catch (NoSuchConversationException)
            {
                conversation = null;
            }
        }

        /// <summary>
        /// Promote the current conversation from a transient conversation (which will persist to the
        /// end of the next request) to a non-transient conversation (which will persist until
        /// the conversation is explicitly ended by calling Conversation.End()).  This method
        /// only works once; subsequent calls have no effect.
        /// </summary>
        public static void Begin(Type flow = null)
        {
            Current.Begin(flow);
        }

        /// <summary>
        /// Change the ID of the current conversation to the provided ID and promote the conversation 
        /// from a transient conversation (which will persist to the end of the next request) to 
        /// a non-transient conversation (which will persist until the conversation is explicitly 
        /// ended by calling Conversation.End()).  This method only works once; subsequent calls 
        /// have no effect.
        /// </summary>
        /// <param name="id"></param>
        public static void Begin(Guid id, Type flow = null)
        {
            Current.Begin(id, flow);
        }

        /// <summary>
        /// Demote the current conversation from a non-transient conversation to a transient conversation
        /// so that it will end at the completion of this response.  This method only works once;
        /// subsequent calls have no effect.
        /// </summary>
        public static void End()
        {
            Current.End();
        }

        /// <summary>
        /// Return a class that stores or retrieves a single value of the specified type
        /// in the current conversation.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ConversationValue<T> Model<T>()
        {
            return Current.Model<T>();
        }
    }
}