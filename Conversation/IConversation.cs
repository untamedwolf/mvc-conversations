namespace Stryker.Conversations
{
    using System;

    /// <summary>
    /// Represents an instance of a bag of values whose lifecycle spans multiple requests; the precise
    /// lifecycle is explicitly demarcated either programmatically or declaratively by
    /// the use of attributes on Action methods.
    /// </summary>
    public interface IConversation
    {
        /// <summary>
        /// Gets or sets the Type marker for this conversation.
        /// </summary>
        Type Flow { get; set; }

        /// <summary>
        /// Gets the unique identifier of the current conversation
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Gets or sets the amount of time this conversation may be left untouched before it
        /// is garbage-collected.
        /// </summary>
        TimeSpan TimeOut { get; set; }

        /// <summary>
        /// Gets a value indicating whether the current conversation will end at 
        /// the completion of the next response; or if the conversation will
        /// only end when Conversation.End() is called.
        /// </summary>
        bool Transient { get; }

        /// <summary>
        /// An key-value dictionary that will persist as long as this conversation
        /// is considered active.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        object this[string key] { get; set; }
        
        /// <summary>
        /// Change the ID of this conversation to the provided ID and promote this conversation 
        /// from a transient conversation (which will persist to the end of the next request) to 
        /// a non-transient conversation (which will persist until the conversation is explicitly 
        /// ended by calling Conversation.End()).  This method only works once; subsequent calls 
        /// have no effect.
        /// </summary>
        /// <param name="id"></param>
        void Begin(Guid id, Type flow = null);

        /// <summary>
        /// Promote this conversation from a transient conversation (which will persist to the
        /// end of the next request) to a non-transient conversation (which will persist until
        /// the conversation is explicitly ended by calling Conversation.End()).  This method
        /// only works once; subsequent calls have no effect.
        /// </summary>
        void Begin(Type flow = null);

        /// <summary>
        /// Demote this conversation from a non-transient conversation to a transient conversation
        /// so that it will end at the completion of this response.  This method only works once;
        /// subsequent calls have no effect.
        /// </summary>
        void End();

        /// <summary>
        /// Return a class that stores or retrieves a single value of the specified type
        /// in this conversation.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        ConversationValue<T> Model<T>();
    }
}
