﻿namespace Stryker.Conversations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Stryker.Conversations.Configuration;

    /// <summary>
    /// Class used to store or retrieve a single value of a specified type in
    /// the conversation passed to the constructor.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ConversationValue<T>
    {
        /// <summary>
        /// The conversation from which the value is to be retrieved
        /// </summary>
        private IConversation conversation;

        /// <summary>
        /// Initializes a new instance of the <see cref="Stryker.Conversations.ConversationValue&lt;T&gt;"/> class that 
        /// stores Value in the specified conversation.
        /// </summary>
        /// <param name="conversation"></param>
        public ConversationValue(IConversation conversation)
        {
            this.conversation = conversation;
        }

        /// <summary>
        /// Gets or sets a value of the specified type in the conversation
        /// </summary>
        public T Value
        {
            get
            {
                string key = string.Format(ConversationConfiguration.CONVERSATION_TYPE_TEMPLATE, typeof(T).AssemblyQualifiedName);

                if (this.conversation[key] is T)
                {
                    return (T)this.conversation[key];
                }

                if (this.conversation[key] != null)
                {
                    throw new InvalidOperationException(string.Format("Conversation expected type to be [{0}] but found [{1}]", typeof(T).AssemblyQualifiedName, this.conversation[key].GetType().AssemblyQualifiedName));
                }

                return default(T);
            }

            set
            {
                this.conversation[string.Format(ConversationConfiguration.CONVERSATION_TYPE_TEMPLATE, typeof(T).AssemblyQualifiedName)] = value;
            }
        }
    }
}
