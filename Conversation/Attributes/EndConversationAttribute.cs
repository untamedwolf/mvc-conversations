﻿namespace Stryker.Conversations.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// <para>
    /// Demote the current conversation to a transient conversation after
    /// executing the action.
    /// </para>
    /// <para>
    /// Functionally equivalent to call Conversation.End() at the end
    /// of the action.
    /// </para>
    /// </summary>
    public class EndConversationAttribute : ActionFilterAttribute, IActionFilter
    {
        /// <summary>
        /// Demote the current conversation to a non-transient conversation.
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Conversation.End();
        }
    }
}