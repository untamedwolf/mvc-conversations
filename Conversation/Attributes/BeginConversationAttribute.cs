﻿namespace Stryker.Conversations.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// <para>
    /// Promote the current conversation to a non-transient conversation before
    /// executing the action.
    /// </para>
    /// <para>
    /// Functionally equivalent to call Conversation.Begin() at the start
    /// of the action.
    /// </para>
    /// </summary>
    public class BeginConversationAttribute : ActionFilterAttribute, IActionFilter
    {
        /// <summary>
        /// Gets or sets an optional type marker that will be added to the conversation
        /// </summary>
        public Type Flow { get; set; }

        /// <summary>
        /// Promote the current conversation to a non-transient conversation.
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Conversation.Begin(this.Flow);
        }
    }
}