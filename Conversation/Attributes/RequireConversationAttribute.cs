﻿namespace Stryker.Conversations.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Stryker.Conversations.Exception;

    /// <summary>
    /// <para>
    /// Mark an Action Method with this attribute if a conversation is required
    /// to execute the action.  If an Action is specified then the user will
    /// be redirected to the Controller/Action if no conversation is specified;
    /// if no Action is specified a NoConversationException will be thrown
    /// if no conversation is specified.
    /// </para>
    /// <para>
    /// NOTE: If a conversation is specified but the conversation does not exist
    /// (i.e. due to timeout) a NoSuchConversationException will continue to be
    /// thrown.
    /// </para>
    /// </summary>
    public class RequireConversationAttribute : ActionFilterAttribute, IActionFilter
    {
        /// <summary>
        /// Gets or sets the Action to redirect to if no conversation is open
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Gets or sets the Controller to redirect to if no conversation is open
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// Gets or sets a Type marker that the Conversation must match; if the
        /// current conversation does not match the Type marker a
        /// WrongFlowException will be thrown.
        /// </summary>
        public Type Flow { get; set; }

        /// <summary>
        /// <para>
        /// Redirect the user (or throw NoConversationException) if no conversation
        /// has been specified.
        /// </para>
        /// <para>
        /// Throw a WrongFlowException if the wrong flow is current being used.
        /// </para>
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Conversation.Current == null || Conversation.Transient)
            {
                if (!string.IsNullOrEmpty(this.Action))
                {
                    RouteValueDictionary routeValues = new RouteValueDictionary();
                    routeValues["Action"] = this.Action;
                    routeValues["Controller"] = this.Controller;

                    filterContext.Result = new RedirectToRouteResult(routeValues);
                }
                else
                {
                    throw new NoConversationException();
                }
            }
            else
            {
                if (
              this.Flow != null
              &&
              (
                  Conversation.Flow == null
                  ||
                  !this.Flow.Equals(Conversation.Flow))
              )
                {
                    throw new WrongFlowException(this.Flow, Conversation.Flow);
                }
            }
        }
    }
}