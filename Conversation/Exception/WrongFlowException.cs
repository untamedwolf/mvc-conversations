﻿namespace Stryker.Conversations.Exception
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This exception is thrown when an attempt to start an Action with the wrong type of flow
    /// is made.
    /// </summary>
    [Serializable]
    public class WrongFlowException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WrongFlowException"/> class.
        /// </summary>
        public WrongFlowException(Type flow, Type actual)
                    : base(string.Format("Wrong flow in process; needed {0} flow but have {1} flow", flow.AssemblyQualifiedName, actual == null ? "no" : actual.AssemblyQualifiedName))
        {
        }
    }
}
