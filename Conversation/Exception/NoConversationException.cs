﻿namespace Stryker.Conversations.Exception
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// This error is thrown if an action is annotated [RequireConversation] but
    /// the current conversation is transient.
    /// </summary>
    [Serializable]
    public class NoConversationException : Exception
    {
    }
}