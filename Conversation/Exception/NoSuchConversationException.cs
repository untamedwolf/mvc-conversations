﻿namespace Stryker.Conversations.Exception
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// <para>
    /// This exception is thrown if the conversation specified by the
    /// request parameter does not resolve to a non-transient conversation
    /// stored in the session.
    /// </para>
    /// <para>
    /// This is most likely caused either by the conversation having timed
    /// out or the user guessing at conversation ids.
    /// </para>
    /// </summary>
    [Serializable]
    public class NoSuchConversationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoSuchConversationException"/> class.
        /// </summary>
        public NoSuchConversationException() : base() 
        { 
        }
    }
}