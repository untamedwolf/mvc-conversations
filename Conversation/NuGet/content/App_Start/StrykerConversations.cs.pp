using System;
using System.Web.Mvc;
using Stryker.Conversations.Web;

[assembly: WebActivatorEx.PostApplicationStartMethod(
    typeof($rootnamespace$.App_Start.StrykerConversations), "PostStart")]

namespace $rootnamespace$.App_Start {
    public static class StrykerConversations {
        public static void PostStart() {
            GlobalFilters.Filters.Add(new ConversationFilter());
        }
    }
}