﻿namespace Stryker.Conversations.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;    
    using Stryker.Conversations.Configuration;
    using Conversations = Stryker.Conversations;

    /// <summary>
    /// Extensions to HtmlHelper to make it easier to use conversations with views
    /// </summary>
    public static class HtmlHelperExtensions
    {
        /// <summary>
        /// Ensures the conversation is propagated with the form (Create a hidden field that contains the current conversation id).
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        public static MvcHtmlString Conversation(this HtmlHelper helper)
        {
            return helper.Hidden(ConversationConfiguration.REQUEST_PARAMETER, Conversations.Conversation.Id.ToString());
        }

        /// <summary>
        /// Generate an ActionLink that propagates the current conversation.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="protocol"></param>
        /// <param name="hostName"></param>
        /// <param name="fragment"></param>
        /// <param name="routeValues"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString ConversationLink(this HtmlHelper helper, string linkText, string actionName, string controllerName, string protocol, string hostName, string fragment, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            if (!Conversations.Conversation.Transient)
            {
                if (routeValues == null)
                {
                    routeValues = new RouteValueDictionary();
                }

                routeValues.Add(ConversationConfiguration.REQUEST_PARAMETER, Conversations.Conversation.Id);
            }

            return helper.ActionLink(linkText, actionName, controllerName, protocol, hostName, fragment, routeValues, htmlAttributes);
        }

        /// <summary>
        /// Generate an ActionLink that propagates the current conversation.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="protocol"></param>
        /// <param name="hostName"></param>
        /// <param name="fragment"></param>
        /// <param name="routeValues"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString ConversationLink(this HtmlHelper helper, string linkText, string actionName, string controllerName, string protocol, string hostName, string fragment, object routeValues, object htmlAttributes)
        {
            RouteValueDictionary routeValueDict = null;
            IDictionary<string, object> htmlAttributeDict = null;

            if (routeValues != null)
            {
                routeValueDict = new RouteValueDictionary();
                var paramRouteValues = AsDictionary(routeValues);

                foreach (string key in paramRouteValues.Keys)
                {
                    routeValueDict.Add(key, paramRouteValues[key]);
                }
            }

            if (htmlAttributes != null)
            {
                htmlAttributeDict = new Dictionary<string, object>();
                var paramHtmlAttributes = AsDictionary(htmlAttributes);

                foreach (string key in paramHtmlAttributes.Keys)
                {
                    htmlAttributeDict.Add(key, paramHtmlAttributes[key]);
                }
            }

            return ConversationLink(helper, linkText, actionName, controllerName, protocol, hostName, fragment, routeValueDict, htmlAttributeDict);
        }

        /// <summary>
        /// Generate an ActionLink that propagates the current conversation.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="routeValues"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString ConversationLink(this HtmlHelper helper, string linkText, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            return ConversationLink(helper, linkText, actionName, controllerName, null, null, null, routeValues, htmlAttributes);
        }

        /// <summary>
        /// Generate an ActionLink that propagates the current conversation.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <param name="routeValues"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString ConversationLink(this HtmlHelper helper, string linkText, string actionName, string controllerName, object routeValues, object htmlAttributes)
        {
            return ConversationLink(helper, linkText, actionName, controllerName, null, null, null, routeValues, htmlAttributes);
        }

        /// <summary>
        /// Generate an ActionLink that propagates the current conversation.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="routeValues"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString ConversationLink(this HtmlHelper helper, string linkText, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            return ConversationLink(helper, linkText, actionName, null, routeValues, htmlAttributes);
        }

        /// <summary>
        /// Generate an ActionLink that propagates the current conversation.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="routeValues"></param>
        /// <param name="htmlAttributes"></param>
        /// <returns></returns>
        public static MvcHtmlString ConversationLink(this HtmlHelper helper, string linkText, string actionName, object routeValues, object htmlAttributes)
        {
            return ConversationLink(helper, linkText, actionName, null, routeValues, htmlAttributes);
        }

        /// <summary>
        /// Generate an ActionLink that propagates the current conversation.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <returns></returns>
        public static MvcHtmlString ConversationLink(this HtmlHelper helper, string linkText, string actionName, string controllerName)
        {
            return ConversationLink(helper, linkText, actionName, controllerName, null, null);
        }

        /// <summary>
        /// Generate an ActionLink that propagates the current conversation.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="routeValues"></param>
        /// <returns></returns>
        public static MvcHtmlString ConversationLink(this HtmlHelper helper, string linkText, string actionName, RouteValueDictionary routeValues)
        {
            return ConversationLink(helper, linkText, actionName, routeValues, null);
        }

        /// <summary>
        /// Generate an ActionLink that propagates the current conversation.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <param name="routeValues"></param>
        /// <returns></returns>
        public static MvcHtmlString ConversationLink(this HtmlHelper helper, string linkText, string actionName, object routeValues)
        {
            return ConversationLink(helper, linkText, actionName, routeValues, null);
        }

        /// <summary>
        /// Generate an ActionLink that propagates the current conversation.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="linkText"></param>
        /// <param name="actionName"></param>
        /// <returns></returns>
        public static MvcHtmlString ConversationLink(this HtmlHelper helper, string linkText, string actionName)
        {
            return ConversationLink(helper, linkText, actionName, null, null);
        }

        /// <summary>
        /// Convert an object with properties into a dictionary.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="bindingAttr"></param>
        /// <returns></returns>
        public static IDictionary<string, object> AsDictionary(object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            return source.GetType().GetProperties(bindingAttr).ToDictionary(
                propInfo => propInfo.Name,
                propInfo => propInfo.GetValue(source, null));
        }
    }
}