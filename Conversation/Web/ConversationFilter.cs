﻿namespace Stryker.Conversations.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Stryker.Conversations.Configuration;

    /// <summary>
    /// <para>
    /// This filter creates a conversation for the current request; and at
    /// the end of each request removes any expired conversations.
    /// </para>
    /// <para>
    /// This filter should be installed as a global filter in Global.ascx.cs
    /// </para>
    /// </summary>
    public class ConversationFilter : IActionFilter
    {
        /// <summary>
        /// Removes all expired conversations from the session.
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            this.RemoveExpiredConversations(filterContext);
            this.AugmentRedirects(filterContext);
        }

        /// <summary>
        /// Create a conversation for this request and place it's ID in the ViewBag
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // initialize conversation
            HttpContext.Current.Items[ConversationConfiguration.HTTPCONTEXT_ITEM] = new ConversationInstance(filterContext.Controller);

            // put conversation id in view bag
            filterContext.Controller.ViewData[ConversationConfiguration.CURRENT_CONVERSATION] = Conversation.Id;
        }

        /// <summary>
        /// Remove conversations that have expired from the session
        /// </summary>
        /// <param name="filterContext"></param>
        protected void RemoveExpiredConversations(ActionExecutedContext filterContext)
        {
            // clean up conversations
            var session = HttpContext.Current.Session;

            // find all conversations not modified since expiry window
            var lastAccessIds = session[ConversationConfiguration.CONVERSATION_ACTIVE_TEMPLATE] is Dictionary<Guid, DateTime>
                                    ?
                                    (session[ConversationConfiguration.CONVERSATION_ACTIVE_TEMPLATE] as Dictionary<Guid, DateTime>).Keys.ToList()
                                    :
                                    new List<Guid>();

            // find relation session data
            var removeKeys = new List<string>();
            foreach (Guid conversationId in lastAccessIds)
            {
                // determine if timedout
                DateTime lastAccessed = (DateTime)(session[ConversationConfiguration.CONVERSATION_ACTIVE_TEMPLATE] as Dictionary<Guid, DateTime>)[conversationId];
                TimeSpan timeout = (TimeSpan)session[string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, conversationId, ConversationConfiguration.CONVERSATION_TIMEOUT)];

                // if timed out mark conversation data for removal
                if (lastAccessed.Add(timeout) < DateTime.Now)
                {
                    HashSet<string> usedKeys =
                        session[
                            string.Format(
                                ConversationConfiguration.CONVERSATION_STORE_TEMPLATE,
                                conversationId,
                                ConversationConfiguration.CONVERSATION_USEDKEYS)
                        ] as HashSet<string>;

                    // remove used keys
                    if (usedKeys != null)
                    {
                        var sessionKeys = 
                            from string s in usedKeys 
                            select string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, conversationId, s);

                        removeKeys.AddRange(sessionKeys.ToList());
                    }

                    // remove storage for used keys
                    removeKeys.Add(
                        string.Format(
                                ConversationConfiguration.CONVERSATION_STORE_TEMPLATE,
                                conversationId,
                                ConversationConfiguration.CONVERSATION_USEDKEYS)
                    );

                    // remove from active conversation list
                    if (session[ConversationConfiguration.CONVERSATION_ACTIVE_TEMPLATE] is Dictionary<Guid, DateTime>)
                    {
                        (session[ConversationConfiguration.CONVERSATION_ACTIVE_TEMPLATE] as Dictionary<Guid, DateTime>).Remove(conversationId);
                    }
                }                
            }            

            // clear session data associated with the conversations
            foreach (string key in removeKeys)
            {
                session.Remove(key);
            }
        }

        /// <summary>
        /// Add the conversation id to any Action Result that is a temporary redirect
        /// (provided that the current conversation is non-transient).
        /// </summary>
        /// <param name="filterContext"></param>
        protected void AugmentRedirects(ActionExecutedContext filterContext)
        {
            // add conversation id parameter for redirect results

            // we only need to do this if the conversation is permanent
            // and if we intend to propagate the conversation
            if (!Conversation.Transient && Conversation.PropagateOnRedirect)
            {
                // if this is a simple redirect
                if (filterContext.Result is RedirectResult)
                {
                    RedirectResult result = (RedirectResult)filterContext.Result;

                    // don't augment permanent redirects with conversations
                    // conversation aren't permanent by their very nature
                    if (!result.Permanent)
                    {
                        // build conversation request parameter
                        string additionalParam = ConversationConfiguration.REQUEST_PARAMETER + "=" + Conversation.Id;

                        // add the parameter to the URL
                        if (result.Url.Contains('?'))
                        {
                            filterContext.Result = new RedirectResult(result.Url + "&" + additionalParam, false);
                        }
                        else
                        {
                            filterContext.Result = new RedirectResult(result.Url + "?" + additionalParam, false);
                        }
                    }
                }

                // if this is a route redirect
                if (filterContext.Result is RedirectToRouteResult)
                {
                    RedirectToRouteResult result = (RedirectToRouteResult)filterContext.Result;

                    // don't augment permanent redirects with conversations
                    // conversation aren't permanent by their very nature
                    if (!result.Permanent)
                    {
                        // copy route values from current request
                        RouteValueDictionary routeValues = new RouteValueDictionary();
                        if (result.RouteValues != null)
                        {
                            foreach (string key in result.RouteValues.Keys)
                            {
                                routeValues.Add(key, result.RouteValues[key]);
                            }
                        }

                        // add route value for conversation parameter
                        routeValues.Add(ConversationConfiguration.REQUEST_PARAMETER, Conversation.Id);

                        // create new result for the route redirect
                        filterContext.Result = new RedirectToRouteResult(result.RouteName, routeValues, false);
                    }
                }
            }
        }
    }
}