namespace Stryker.Conversations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using Stryker.Conversations.Configuration;
    using Stryker.Conversations.Exception;
    
    /// <summary>
    /// Represents an instance of a bag of values whose lifecycle spans multiple requests; the precise
    /// lifecycle is explicitly demarcated either programmatically or declaratively by
    /// the use of attributes on Action methods.
    /// </summary>
    public class ConversationInstance : IConversation
    {
        /// <summary>
        /// The unique ID assigned to this conversation
        /// </summary>
        private Guid id = Guid.Empty;
        
        /// <summary>
        /// Whether the conversation is currently transient or non-transient
        /// </summary>
        private bool transient = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="Stryker.Conversations.ConversationInstance"/> class
        /// the current request.  If a conversation id is provided as a request parameter then this request 
        /// will join that conversation; if no conversation id is provided a new transient conversation 
        /// will begin.
        /// </summary>
        /// <param name="controller"></param>
        public ConversationInstance(ControllerBase controller)
        {
            // get conversation parameter if exists
            Guid proposedId = Guid.Empty;
            string param = HttpContext.Current.Request.Params[ConversationConfiguration.REQUEST_PARAMETER];

            if (!string.IsNullOrEmpty(param))
            {
                string[] paramSplit = param.Split(new char[] { ',' }, 2);
                Guid.TryParse(paramSplit[0], out proposedId);
            }

            if (!proposedId.Equals(Guid.Empty))
            {
                // if conversation parameter exists
                this.Initialize(controller, proposedId, false);
            }
            else
            {
                // if conversation parameter doesn't exist
                this.Initialize(controller, Guid.NewGuid(), true);
            }

            // set the timeout to the default
            ((IConversation)this).TimeOut = ConversationConfiguration.EXPIRY;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Stryker.Conversations.ConversationInstance"/> class
        /// with an arbitrary conversationId.  If a conversation with the id already exists then this will 
        /// return that conversation; otherwise if create is true a transient conversation with that id will 
        /// be created; otherwise if create is false a NoSuchConversationException will be thrown.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="conversationId"></param>
        /// <param name="create"></param>
        public ConversationInstance(ControllerBase controller, Guid conversationId, bool create)
        {
            this.Initialize(controller, conversationId, create);
        }

        /// <summary>
        /// Gets or sets the Type marker for this conversation.
        /// </summary>
        public Type Flow
        {
            get
            {
                string value = this[ConversationConfiguration.CONVERSATION_FLOW] as string;
                if (!string.IsNullOrEmpty(value))
                {
                    return Type.GetType(value);
                }

                return null;
            }

            set
            {
                this[ConversationConfiguration.CONVERSATION_FLOW] = value != null ? value.AssemblyQualifiedName : null;
            }
        }

        /// <summary>
        /// Gets the unique identifier of this conversation
        /// </summary>
        public Guid Id
        {
            get
            {
                return this.id;
            }
        }

        /// <summary>
        /// Gets or sets the amount of time this conversation may be left untouched before it
        /// is garbage-collected.
        /// </summary>
        public TimeSpan TimeOut
        {
            get
            {
                return (TimeSpan)this[ConversationConfiguration.CONVERSATION_TIMEOUT];
            }

            set
            {
                this[ConversationConfiguration.CONVERSATION_TIMEOUT] = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the current conversation will end at 
        /// the completion of the next response; or if the conversation will
        /// only end when Conversation.End() is called.
        /// </summary>
        public bool Transient
        {
            get
            {
                return this.transient;
            }
        }

        /// <summary>
        /// Gets or sets the Controller that this conversation instance was created for.  
        /// Should not be used by user classes.
        /// </summary>
        internal ControllerBase Controller { get; set; }

        /// <summary>
        /// An key-value dictionary that will persist as long as this conversation
        /// is considered active.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object this[string key]
        {
            get
            {                
                if (this.transient)
                {
                    // if conversation is transient
                    // retrieve data from viewdata
                    return this.Controller.ViewData[string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, this.id, key)];
                }                
                else
                {
                    // if conversation is not transient
                    // retrieve data from session
                    var session = HttpContext.Current.Session;
                    return session[string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, this.id, key)];
                }
            }

            set
            {
                // store this as a used key
                if (key != ConversationConfiguration.CONVERSATION_USEDKEYS)
                {
                    if (!(this[ConversationConfiguration.CONVERSATION_USEDKEYS] is HashSet<string>))
                    {
                        this[ConversationConfiguration.CONVERSATION_USEDKEYS] = new HashSet<string>();
                    }

                    (this[ConversationConfiguration.CONVERSATION_USEDKEYS] as HashSet<string>).Add(key);
                }

                if (this.transient)
                {
                    // if conversation is transient
                    // store data in viewdata
                    this.Controller.ViewData[string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, this.id, key)] = value;
                }                
                else
                {
                    // if conversation is not transient
                    // store data in session
                    var session = HttpContext.Current.Session;
                    session[string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, this.id, key)] = value;
                }
            }
        }

        /// <summary>
        /// Change the ID of this conversation to the provided ID and promote this conversation 
        /// from a transient conversation (which will persist to the end of the next request) to 
        /// a non-transient conversation (which will persist until the conversation is explicitly 
        /// ended by calling Conversation.End()).  This method only works once; subsequent calls 
        /// have no effect.
        /// </summary>
        /// <param name="id"></param>
        public void Begin(Guid id, Type flow = null)
        {
            // if conversation is transient
            if (this.transient)
            {
                var session = HttpContext.Current.Session;

                var usedKeys = this[ConversationConfiguration.CONVERSATION_USEDKEYS] as HashSet<string>;
                usedKeys = usedKeys==null ? new HashSet<string>() : usedKeys;

                // get conversation viewdata keys
                var keys = (from
                               string key in usedKeys
                            select string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, this.id, key))
                           .ToList();

                // copy conversation viewdata to session data (changing id if necessary)
                foreach (string key in keys)
                {
                    string[] segments = key.Split(new char[] { ConversationConfiguration.CONVERSATION_STORE_SEPARATOR }, 3);
                    if (segments.Length == 3)
                    {
                        string keyTerm = segments[2];
                        session[string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, id, keyTerm)] = this.Controller.ViewData[key];
                    }
                }

                // remove all conversation viewdata
                foreach (string key in keys)
                {
                    this.Controller.ViewData.Remove(key);
                }
                this.Controller.ViewData.Remove(string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, id, ConversationConfiguration.CONVERSATION_USEDKEYS));

                // set last access time of conversation to now
                (HttpContext.Current.Session[ConversationConfiguration.CONVERSATION_ACTIVE_TEMPLATE]
                    as Dictionary<Guid, DateTime>)[id] = DateTime.Now;

                // set transient to false
                this.transient = false;

                // update id to provided parameter
                this.id = id;

                this.Flow = flow;
            }
        }

        /// <summary>
        /// Promote this conversation from a transient conversation (which will persist to the
        /// end of the next request) to a non-transient conversation (which will persist until
        /// the conversation is explicitly ended by calling Conversation.End()).  This method
        /// only works once; subsequent calls have no effect.
        /// </summary>
        public void Begin(Type flow = null)
        {
            this.Begin(this.id, flow);
        }

        /// <summary>
        /// Demote this conversation from a non-transient conversation to a transient conversation
        /// so that it will end at the completion of this response.  This method only works once;
        /// subsequent calls have no effect.
        /// </summary>
        public void End()
        {
            // if conversation is not transient
            if (!this.transient)
            {
                var session = HttpContext.Current.Session;

                var usedKeys = this[ConversationConfiguration.CONVERSATION_USEDKEYS] as HashSet<string>;
                usedKeys = usedKeys == null ? new HashSet<string>() : usedKeys;

                // get conversation session keys
                var keys = (from
                               string key in usedKeys
                            select string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, this.id, key))
                           .ToList();

                // copy conversation session data to viewdata
                foreach (string key in keys)
                {
                    string[] segments = key.Split(new char[] { ConversationConfiguration.CONVERSATION_STORE_SEPARATOR }, 3);
                    if (segments.Length == 3)
                    {
                        string keyTerm = segments[2];
                        this.Controller.ViewData[string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, this.id, keyTerm)] = session[key];
                    }
                }

                // remove all conversation session data
                foreach (string key in keys)
                {
                    session.Remove(key);
                }
                session.Remove(string.Format(ConversationConfiguration.CONVERSATION_STORE_TEMPLATE, this.id, ConversationConfiguration.CONVERSATION_USEDKEYS));

                // remove from list of active conversations
                (HttpContext.Current.Session[ConversationConfiguration.CONVERSATION_ACTIVE_TEMPLATE]
                    as Dictionary<Guid, DateTime>).Remove(this.id);

                // set transient to true
                this.transient = true;
            }
        }

        /// <summary>
        /// Return a class that stores or retrieves a single value of the specified type
        /// in this conversation.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public ConversationValue<T> Model<T>()
        {
            return new ConversationValue<T>(this);
        }

        /// <summary>
        /// Shared initialization code from the constructors; initializes the created
        /// conversation to have the specified id; if create is false and a non-transient
        /// conversation with the specified id does not already exist this method throws
        /// a NoSuchConversationException.  If a non-transient conversation with that
        /// id already exists this object becomes that conversation; and if it doesn't
        /// and create is set to true then this object becomes a new transient conversation
        /// with the specified conversationId.
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="conversationId"></param>
        /// <param name="create"></param>
        private void Initialize(ControllerBase controller, Guid conversationId, bool create)
        {
            this.Controller = controller;

            Dictionary<Guid, DateTime> activeConversations = HttpContext.Current.Session[ConversationConfiguration.CONVERSATION_ACTIVE_TEMPLATE] as Dictionary<Guid, DateTime>;
            if (activeConversations == null)
            {
                activeConversations = new Dictionary<Guid, DateTime>();
                HttpContext.Current.Session[ConversationConfiguration.CONVERSATION_ACTIVE_TEMPLATE] = activeConversations;
            }

            if (activeConversations.Keys.Contains(conversationId))
            {
                // if conversation exists                
                // set transient to false
                this.transient = false;

                // update access time for conversation
                (HttpContext.Current.Session[ConversationConfiguration.CONVERSATION_ACTIVE_TEMPLATE] 
                    as Dictionary<Guid, DateTime>)[conversationId] = DateTime.Now;
                
                // set conversation id to this coversation parameter
                this.id = conversationId;
            }            
            else
            {
                // if conversation doesn't exist
                if (create)
                {
                    this.id = conversationId;
                    this.transient = true;
                }
                else
                {
                    // throw a conversation not exists exception
                    throw new NoSuchConversationException();
                }
            }
        }
    }
}
